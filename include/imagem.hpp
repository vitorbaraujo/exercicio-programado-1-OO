#ifndef IMAGEM_H
#define IMAGEM_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

class Imagem{
	private:
		int altura;
		int largura;
		int valCinza;
		int *matriz_aux;
		
	public:
/*	construtores	*/
		Imagem();
		Imagem(const char nome_arq[]);
		Imagem(int altura,int largura,int valCinza);
/*	gets	*/		
		int getAltura();
		int getLargura();
		int getGray();
/*	sets	*/
		void setAltura(int altura);
		void setLargura(int largura);
		void setGray(int valCinza);
/*	outros metodos	*/
		void leiaImagem(const char nome_arq[]);
		void salvaImagem(const char novo_arq[]);
		
		int getPixel(int i,int j);
		void alteraPixel(int i,int j,int novoPixel);
};

#endif
		
