#ifndef SHARPEN_H
#define SHARPEN_H

#include "imagem.hpp"
#include "filtro.hpp"

#include <iostream>

using namespace std;

class Sharpen : public Filtro{
	public:
		Sharpen();
		
		void aplicaFiltro(Imagem &umaImagem);
};

#endif
		
		
