#ifndef FILTRO_H
#define FILTRO_H

#include "imagem.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

class Filtro{
	public:
/*	construtor	*/
		Filtro();

/*	metodo virtual, ou seja, deve ser implementado nas funcoes filhas	*/
		virtual void aplicaFiltro() { };
		
};

#endif		
	
