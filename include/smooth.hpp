#ifndef SMOOTH_H
#define SMOOTH_H

#include "imagem.hpp"
#include "filtro.hpp"

#include <iostream>

using namespace std;

class Smooth : public Filtro{
	public:
/*	Construtor	*/
		Smooth();
		
		void aplicaFiltro(Imagem &umaImagem);
};

#endif
