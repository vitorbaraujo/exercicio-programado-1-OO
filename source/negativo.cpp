#include "negativo.hpp"
#include "filtro.hpp"

using namespace std;

Negativo::Negativo(){
	
}

void Negativo::aplicaFiltro(Imagem &umaImagem){
	int i,j,aux;
	
	for(i=0;i<umaImagem.getAltura();i++){
		for(j=0;j<umaImagem.getLargura();j++){
			aux = 255 - umaImagem.getPixel(i,j);
			umaImagem.alteraPixel(i,j,aux);
		}
	}		
	
	cout << endl << "Negativo OK" << endl;	
}
