#include "imagem.hpp"
#include "sharpen.hpp"

#include <iostream>

using namespace std;

Sharpen::Sharpen(){
	
}

void Sharpen::aplicaFiltro(Imagem &umaImagem){
	int i,j,aux,num,x,y,div,size;
	int sharpen[]={0,-1,0,-1,5,-1,0,-1,0};
	int *m_aux_sh=new int[umaImagem.getAltura()*umaImagem.getLargura()];
	
	div=1;
	size=3;
	aux=size/2;
	
	for(i=aux;i<umaImagem.getAltura()-aux;i++){
		for(j=aux;j<umaImagem.getLargura()-aux;j++){
			num=0;
			
			for(x=-1;x<=1;x++){
				for(y=-1;y<=1;y++){
					num+=sharpen[(x+1)+size*(y+1)]*umaImagem.getPixel(i+x,y+j);
				}
			}
		
			num=num/div;
		
			num=num < 0 ? 0 : num;
			num=num > 255 ? 255: num;
		
			m_aux_sh[i+umaImagem.getLargura()*j]=num;
		}
	}
	
	for(i=0;i<umaImagem.getAltura();i++){
		for(j=0;j<umaImagem.getLargura();j++){
			umaImagem.alteraPixel(i,j,m_aux_sh[i+umaImagem.getLargura()*j]);
		}
	}
	
	
	cout << "Sharpen OK" << endl;
}
