#include "imagem.hpp"

using namespace std;

/*	declaracao construtores	*/

Imagem::Imagem(){
	altura=0;
	largura=0;
	valCinza=0;
	
	matriz_aux = NULL;
}

Imagem::Imagem(const char nome_arq[]){	/* Verificacao inicial da imagem	*/
	char cabecalho[100],*ptr;
    ifstream imagem_aux;
	
	imagem_aux.open(nome_arq, ios::in | ios::binary);

	cout << endl << "Lendo imagem..." << endl;
   
    if (!imagem_aux){
		cout << "Nao e possivel abrir a imagem " << nome_arq << endl;
		exit(1);
    }

    imagem_aux.getline(cabecalho,100,'\n');
    if ((cabecalho[0]!=80) || (cabecalho[1]!=53)){   
		cout << "A imagem " << nome_arq << "nao e PGM" << endl;
		exit(1);
    }

    imagem_aux.getline(cabecalho,100,'\n');
    
    while(cabecalho[0]=='#'){
		imagem_aux.getline(cabecalho,100,'\n');
	}
	
    altura=strtol(cabecalho,&ptr,0);
    largura=atoi(ptr);

    imagem_aux.getline(cabecalho,100,'\n');
    valCinza=strtol(cabecalho,&ptr,0);
    matriz_aux=new int[altura*largura];
    
    cout << "Imagem OK" << endl;
}

Imagem::Imagem(int altura,int largura,int valCinza){
	this->altura=altura;
	this->largura=largura;
	this->valCinza=valCinza;
	
	matriz_aux=new int[altura*largura];
}

/*	declaracao gets	*/

int Imagem::getAltura(){
	return altura;
}

int Imagem::getLargura(){
	return largura;
}

int Imagem::getGray(){
	return valCinza;
}

/*	declaracao sets	*/

void Imagem::setAltura(int altura){
	this->altura=altura;
}

void Imagem::setLargura(int largura){
	this->largura=largura;
}

void Imagem::setGray(int valCinza){
	this->valCinza=valCinza;
}

/* declaracao leiaImagem e salvaImagem	*/

void Imagem::leiaImagem(const char nome_arq[]){
	int i,j;
	unsigned char *chImagem;
	char cabecalho[100],*ptr;
	ifstream imagem_aux;
	
	imagem_aux.open(nome_arq, ios::in | ios::binary);	/*	abre imagem_aux com o nome nome_arq	*/
	
/* se o arquivo nao existe, termina o programa	*/	
	if(!imagem_aux){	
		cout << "Nao e possivel abrir a imagem " << nome_arq << endl;
		exit(1);
	}
/* leia nova linha	*/	
	imagem_aux.getline(cabecalho,100,'\n');
	
/* se nao comeca com P5 fecha o programa	*/	
	if((cabecalho[0]!=80) || (cabecalho[1]!=53)){
		cout << "A imagem " << nome_arq << "nao e PGM" << endl;
		exit(1);
	}

/*	leia nova linha	*/
	imagem_aux.getline(cabecalho,100,'\n');
	
/*	leia nova linha enquanto houver comentario	*/
	while(cabecalho[0]=='#'){
		imagem_aux.getline(cabecalho,100,'\n');
	}
	
/*	atribua o tamanho da imagem para altura e largura	*/
	altura=strtol(cabecalho,&ptr,0);
	largura=atoi(ptr);

/*	leia nova linha	*/	
	imagem_aux.getline(cabecalho,100,'\n');

/*	atribua o valor maximo de cinza para valCinza	*/	
	valCinza=strtol(cabecalho,&ptr,0);
	
	chImagem=(unsigned char *) new unsigned char [altura*largura];
	
	imagem_aux.read( reinterpret_cast<char *>(chImagem), (altura*largura)*sizeof(unsigned char));
	
	if(imagem_aux.fail()){
		cout << "A imagem " << nome_arq << "tem tamanho errado" << endl;
		exit(1);
	}
	
	imagem_aux.close();
	
	int val;
	
	for(i=0;i<altura;i++){
		for(j=0;j<largura;j++){
			val=(int)chImagem[i*largura+j];
			matriz_aux[i*largura+j]=val;
		}
	}
	
	delete [] chImagem;
}/* fim leiaImagem	*/

void Imagem::salvaImagem(const char novo_arq[]){
	int i,j;
	unsigned char *chImagem;
	ofstream saida_aux(novo_arq);
	
	chImagem=(unsigned char *) new unsigned char[altura*largura];
	
	int val;
	
	for(i=0;i<altura;i++){
		for(j=0;j<largura;j++){
			val=matriz_aux[i*largura+j];
			chImagem[i*largura+j]=(unsigned char)val;
		}
	}
	
	if(!saida_aux.is_open()){
		cout << "Nao se pode abrir arquivo de saida " << novo_arq << endl;
		exit(1);
	}
	
	saida_aux << "P5" << endl;
	saida_aux << altura << " " << largura << endl;
	saida_aux << 255 << endl;
	
	saida_aux.write(reinterpret_cast<char *>(chImagem), (altura*largura)*sizeof(unsigned char));
	
	cout << "Novo arquivo gerado" << endl;
	saida_aux.close();
}/* fim salvaImagem	*/

int Imagem::getPixel(int i,int j){
		return matriz_aux[i*getLargura()+j];
}

void Imagem::alteraPixel(int i,int j,int novoPixel){
		matriz_aux[i*getLargura()+j]=novoPixel;
}
	
	
	
		 

















