#include "imagem.hpp"
#include "negativo.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"

#include <iostream>

using namespace std;

int main(){
	Negativo objNegativo;
	Sharpen objSharpen;
	Smooth objSmooth;
	Imagem objImagem("../lena.pgm");
	
	
	objImagem.leiaImagem("../lena.pgm");
	
/*	Filtro Negativo	*/	
	objNegativo.aplicaFiltro(objImagem);
	objImagem.salvaImagem("../negLena.pgm");
		
/*	Lendo imagem de origem	*/
	objImagem.leiaImagem("../lena.pgm");
	
/*	Filtro Sharpen	*/
	objSharpen.aplicaFiltro(objImagem);
	objImagem.salvaImagem("../sharpLena.pgm");
		
/*	Lendo imagem de origem	*/	
	objImagem.leiaImagem("../lena.pgm");
	
/*	Filtro Smooth	*/
	objSmooth.aplicaFiltro(objImagem);
	objImagem.salvaImagem("../smoothLena.pgm");
	
	cout << endl << "Fim do programa" << endl << endl;
		
	return 0;
}
