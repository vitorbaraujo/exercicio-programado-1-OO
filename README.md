1:	Digite o comando 'make'	

2:	Certifique-se de que a imagem, de nome 'lena.pgm', deve estar no mesmo diretório da pasta do programa, ou seja, no mesmo diretório devem estar a imagem 'lena.pgm' e a pasta 'exercicio-programado-1-OO'

3:	Caso não ocorra nenhum imprevisto, digite o comando 'make run'

4:	Assim, o programa começará a rodar.

5:	O programa lê a imagem 'lena.pgm' que deve estar no diretório raiz (mesmo diretório da pasta do programa) e aplica, de uma vez, o negativo e os filtros sharpen e smooth e, por fim, salva as imagens respectivas ao negativo e aos filtros na mesma pasta onde a imagem original se encontra.
