SRCFOLDER := source/
INCFOLDER := include/
OBJFOLDER := obj/
BINFOLDER := bin/

CPPC := g++
CPPFLAGS := -Wall -ansi -W

SRCFILES := $(wildcard source/*.cpp)

all:	$(SRCFILES:source/%.cpp=obj/%.o)
		$(CPPC) $(CPPFLAGS) obj/*.o -o bin/finalBinary

obj/%.o:	source/%.cpp
	$(CPPC) $(CPPFLAGS) -c $< -o $@ -I./include

.PHONY:	clean

clean:	
	rm -rf obj/*
	rm -rf bin/*	

run:	
	./bin/finalBinary
	
